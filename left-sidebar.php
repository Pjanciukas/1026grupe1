<?php
							session_start();
							if(count($_SESSION) > 0) {?>
								
						
						

<!DOCTYPE HTML>
<html>
	<head>
		<title>Treniruotės</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<a href="http://www.procycling.lt" target="_blank">
					<img src="images/1511808_501116823379882_7513770146539017981_o.jpg" alt="logo"/>
                </a>
					<nav id="nav">
                    <ul>
                        <li><a href="index.php">APIE</a></li>
                        <li>
                            <a href="#">PASIKALBĖJIMAI</a>
                            <ul>
                                <li><a href="vidinis.php">Dialogai</a></li>
                                <li><a href="#">Gyvenimo būdas</a></li>
                                
                            </ul>
                        </li>
                        
                        <?php
							
							if(count($_SESSION) > 0) {?>
								<a href="logout.php" class="button special">ATSIJUNGTI</a>
						
						<?php } else { ?>
						<li><a href="registration.php" class="button special">REGISTRUOTIS</a></li>
                        <li><a href="login.php" class="button special">PRISIJUNGTI</a></li>
						<?php } ?>
                    </ul>
                </nav>
				</header>

			<!-- Main -->
				<div>
					<div class="container">
						<header class="major">
							<h2>TRENIRUOTĖS</h2>
							<p>MTB ir plento dviračių varžyboms pasiruošti.</p>
						</header>
						<div>
							<div class="homepage">

								<!-- Sidebar -->
									
										<hr />
										<section class="site">
											<a class=""><img src="images/13243892_10154236986092437_4829271521203991631_o.jpg" alt="" /></a>
											<h3>MTB kategorija</h3>
											
											
											 <section id="latest" class="clear borders_MTB">
												<article class="one_quarter"><a href="MTB_treniruotes1.php">
												  <figure><img width="300px" src="images/36306_429439327436_4220306_n.jpg" alt="">
													<figcaption>Pasiruošimas</figcaption>
												  </figure>
												</a></article>
												<article class="one_quarter"><a href="MTB_treniruotes2.php">
												  <figure><img width="300px" src="images/14352415_660874704076214_7899672559994997938_o.jpg" alt="">
													<figcaption>Treniruotės</figcaption>
												  </figure>
												</article>
												<article class="one_quarter"><a href="MTB_treniruotes3.php">
												  <figure><img width="300px" src="images/36306_429439327436_4220306_n.jpg" alt="" />
													<figcaption>Įranga</figcaption>
												  </figure>
												</article>
												
											 </section>
											 
					
											
											<footer>
												<ul class="actions">
													<li><a href="#" class="button">Daugiau</a></li>
												</ul>
											</footer>
										</section>
										

							</div>
							<div class="homepage">

								<!-- Content -->
									<section class="site">
										<a class="image fit"><img src="images/14068481_624518051039758_7462809383720208518_o.jpg" alt="" /></a>
										<h3>PLENTO kategorija</h3>
										<p>Patarimai kaip pasiruošti plento dviračių varžyboms.<p>
										
										   <section id="latest" class="clear borders_MTB">
												<article class="one_quarter"><a href="MTB_treniruotes1.php">
												  <figure><img width="300px" src="images/14068551_320343244965413_1199856102323405357_o.jpg" alt="">
													<figcaption>Treniruotės</figcaption>
												  </figure>
												</a></article>
												<article class="one_quarter"><a href="MTB_treniruotes2.php">
												  <figure><img width="300px" height="199.94px" src="images/13419005_589403211217909_1157954090891251664_n.jpg" alt="">
													<figcaption>Mityba</figcaption>
												  </figure>
												</article>
												<article class="one_quarter"><a href="MTB_treniruotes3.php">
												  <figure><img width="300px" height="199.94px" src="images/14021698_622410241250539_903313921112921864_n.jpg" alt="" />
													<figcaption>Varžybos</figcaption>
												  </figure>
												</article>
												
											 </section>
											 
					
											
											<footer>
												<ul class="actions">
													<li><a href="#" class="button">Daugiau</a></li>
												</ul>
											</footer>
										</section>
										 
									</section>

							</div>
						</div>
					</div>
				</div>

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="https://www.facebook.com/procyclingltteam.lt/?ref=profile" target="_blank" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="https://www.instagram.com/procyclinglt/" target="_blank" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
							<?php } else {
					header ('Location: login.php');	
							} ?>