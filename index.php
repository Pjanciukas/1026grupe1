<!DOCTYPE HTML>
<html>
    <head>
        <title>STAKI</title>
        <meta http-equiv="content-type" content="text/html; charset=utf8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
    </head>
    <body>   
        <div id="page-wrapper">

            <!-- Header -->
            <header id="header">
				<a href="http://www.procycling.lt" target="_blank">
				<img src="images/1511808_501116823379882_7513770146539017981_o.jpg" alt="logo"/>
                </a>
                <nav id="nav">
                    <ul>
                        <li><a href="index.php">APIE</a></li>
                        <li>
                            <a href="#">PASIKALBĖJIMAI</a>
                            <ul>
                                <li><a href="left-sidebar.php">Treniruotės</a></li>
                                <li><a href="vidinis.php">Dialogai</a></li>
                                <li><a href="#">Gyvenimo būdas</a></li>
                                
                            </ul>
                        </li>
						
						<?php
							session_start();
							if(count($_SESSION) > 0) {?>
								<a href="logout.php" class="button special">ATSIJUNGTI</a>
						
						<?php } else { ?>
						
						
                        <li><a href="registration.php" class="button special">REGISTRUOTIS</a></li>
                        <li><a href="login.php" class="button special">PRISIJUNGTI</a></li>
						<?php } ?>
                    </ul>
                </nav>
            </header>

            <!-- Banner -->
            <section id="banner">
                <div class="content">
                    <header>
                        <h2>PRO CYCLING LT BLOG</h2>
                        <p>Ir mėgėjui ir sportininkui!<br />
                           Viskas ką reikia žinoti apie dviračių sportą!<br />
						   Iš profesionalų lūpų!</p>
                    </header>
                    <span class="image"><img src="images/13151473_576278495863714_7024371271242340645_n.jpg" alt="" /></span>
                </div>
            </section>
			
			<!-- One1 -->
			  <!-- content body -->
				

           <!-- One -->
            <section class="spotlight style1 bottom">
               <span class="fit main">
					<video playsinline autoplay muted loop poster="polina.jpg" class="video-pl">
						
						<source src="video/Kontrolines.mp4" type="video/mp4">
					</video>
				
				<!--<img src="images/pic02.jpg" alt="" />--></span>
                <div class="content">

                            <div class="">
                                <header>
                                    <h2>APIE</h2>
                                    <p> <span style="color: green; font-weight: bold;"> Staki – Baltik vairas </span>- profesionalių dviratininkų komanda dalinasi savo žiniomis ir išgyvenimais <br /> 
									ruošiantis varžyboms ir kelionėms dviračiais.
									</p>
                                </header>
                            </div>
                            <div class="">
                                <p>Specialistų rekomendacijos kaip mėgėjui pasiruošti dviračių varžyboms. 
								Sportininkų pasakojimai apie patirtus sunkumus ir džiaugsmo akimirkas.
								</p>
                            </div>
                            <div class="">
                                <p>Tai neformalus, lengvai skaitomas ir kiekvienam suprantamas pasakojimas apie 
								viską kas susiję su dviračiais. </p>
                            </div>

                    </div>
              
            </section>


            <!-- Footer -->
            <footer id="footer">
                <ul class="icons">
                    <li><a href="https://www.facebook.com/procyclingltteam.lt/?ref=profile" target="_blank" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="https://www.instagram.com/procyclinglt/" target="_blank" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
                </ul>
                <ul class="copyright">
                    <li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
                </ul>
            </footer>

        </div>

        <!-- Scripts -->
       <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>