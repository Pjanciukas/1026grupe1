

<html>
    <head>
        <title>Prisijungimas</title>
        <meta http-equiv="content-type" content="text/html; charset=utf8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/else.css" />
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
            <header id="header">
                <h1 id="logo"><a href="index.php"></a></h1>
                <nav id="nav">
                    <ul>
						<li><a href="index.php" class="button special">Pradžia</a></li>
                        <li><a href="registration.php" class="button special">Registracija</a></li>
                    </ul>
                </nav>
            </header>

            <!-- Four -->
            <section id="four" class="wrapper style1 special fade-up">
                <div class="container">
                    <header class="major">
                        <h2>Prašome prisijungti</h2>
                       
                        <form method="post" action="dblogin.php">
                            <div class="row uniform 50%">
                                <div class="text-color 6u 12u$(xsmall)">
                                    <input type="text" name="username" id="username" value="" placeholder="Vartotojo vardas" value maxlength= "10" />
                                </div>
                                <div class="text-color 6u$ 12u$(xsmall)">
                                    <input type="password" name="password" id="password" value="" placeholder="Slaptažodis" value maxlength= "10"/>
                                </div>
                                <div>
                                    <ul class="actions">
                                        <li><input type="submit" value="Login/Prisijungti" class="special" /></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </header>
                </div>
            </section>
            <script>
<!-- Footer -->
                < footer id = "footer" >
                        < ul class = "icons" >
                        < li > < a href = "#" class = "icon alt fa-twitter" > < span class = "label" > Twitter < /span></a > < /li>
                                    < li > < a href = "#" class = "icon alt fa-facebook" > < span class = "label" > Facebook < /span></a > < /li>
                                                < li > < a href = "#" class = "icon alt fa-linkedin" > < span class = "label" > LinkedIn < /span></a > < /li>
                                                            < li > < a href = "#" class = "icon alt fa-instagram" > < span class = "label" > Instagram < /span></a > < /li>
                        < li > < a href = "#" class = "icon alt fa-github" > < s pan class = " label" > GitHub  < /span></a > < /li>
                                                                                    < li > < a href = "#" class = "icon alt fa-envelope" > < span class = "label" > Email < /span></a > < /li>
                                                                                                < /ul>
                                                                                                < ul class = "copyright" >
                                                                                                < li > & copy; Untitled.All rights reserved. < /li><li>Design: <a href="http:/ / html5up.net">HTML5 UP</a></li>
                                                                                                < /ul>
                                                                                                < /footer>

                                                                                                < /div>

<!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/jquery.scrolly.min.js"></script>
            <script src="assets/js/jquery.dropotron.min.js"></script>
            <script src="assets/js/jquery.scrollex.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>

    </body>
</html>